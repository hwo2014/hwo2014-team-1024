{-# LANGUAGE OverloadedStrings #-}
module Message where

import Data.Aeson ((.:), (.:?), decode, encode, (.=), object, FromJSON(..), ToJSON(..), Value(..), Result(..), fromJSON)
import Control.Applicative
import Data.Text (Text)
import Data.Vector (Vector)

data BotMessage = Join Text Text
                | Throttle Float
                | SwitchingLane SwitchMsg
                | Ping
                | CreateTestRace
                | JoinTestRace Text Text Text Text
                  
data ServerMessage = Joined
                   | YourCar CarId
                   | GameInit GameInitData
                   | GameStart
                   | CarPositions [CarPosition]
                   | GameEnd GameResult
                   | TournamentEnd
                   | Crash CarId
                   | Spawn CarId
                   | LapFinished LapResult
                   | DNF DNFData
                   | Finish CarId
                   | Unknown Text
                   | Turbo TurboData  
                   deriving Show
                            
data SwitchMsg = SwitchLeft | SwitchRight
type Reason = Text

data CarId = CarId {
  color     :: Text,
  carIdName :: Text
} deriving (Show, Eq)

instance FromJSON CarId where
  parseJSON (Object v) =
    CarId <$>
    (v .: "color") <*>
    (v .: "name")


data Dimension = Dimension {
  dimensionLength   :: Float,
  width             :: Float,
  guideFlagPosition :: Float
} deriving (Show)

instance FromJSON Dimension where
  parseJSON (Object v) =
    Dimension <$>
    (v .: "length") <*>
    (v .: "width") <*>
    (v .: "guideFlagPosition")

data Car = Car {
  carId         :: CarId,
  dimensions :: Dimension
} deriving (Show)

instance FromJSON Car where
  parseJSON (Object v) =
    Car <$>
    (v .: "id") <*>
    (v .: "dimensions")


data Lane = Lane {
  distanceFromCenter :: Int,
  index              :: Int
} deriving (Show)


instance FromJSON Lane where
  parseJSON (Object v) =
    Lane <$>
    (v .: "distanceFromCenter") <*>
    (v .: "index")
  
data Piece = Straight {
  pieceLength :: Float -> Float,
  switch :: Maybe Bool,
  bridge :: Maybe Bool
  }
           | Bend {
  radius :: Float,
  pieceAngle  :: Float,
  switch :: Maybe Bool,
  bridge :: Maybe Bool,
  pieceLength :: Float -> Float

  }
 deriving (Show)

instance Show ((->) a b) where
  show _ = "Func: <> " 

instance FromJSON Piece where
  parseJSON (Object v) = do
    piece <- (,,,,,)            <$>
             (v .:? "length") <*>
             (v .:? "radius") <*>
             (v .:? "angle")  <*>
             (v .:? "switch") <*>
             (v .:? "bridge") <*>
             return 0
             
    case piece of
      (Just len, Nothing, Nothing, sw, br, _) -> return Straight { pieceLength = const len, switch = sw, bridge = br}
      (Nothing, Just rad, Just ang, sw, br, _) -> return Bend { radius = rad, pieceAngle = ang, switch = sw, bridge = br, pieceLength = \cor -> arcLength (rad+cor) ang}
      _ -> fail "Invalid Piece"

arcLength :: Float -> Float -> Float
arcLength r ang = abs $ (pi * ang * r) / 180

data StartingPoint = StartingPoint {
  position :: Position,
  angle    :: Float
} deriving (Show)

instance FromJSON StartingPoint where
  parseJSON (Object v) =
    StartingPoint <$>
    (v .: "position") <*>
    (v .: "angle")

data Position = Position {
  x :: Float,
  y :: Float
} deriving (Show)

instance FromJSON Position where
  parseJSON (Object v) =
    Position <$>
    (v .: "x") <*>
    (v .: "y")

data Track = Track {
  name          :: Text,
  startingPoint :: StartingPoint,
  pieces        :: Vector Piece,
  lanes         :: Vector Lane
} deriving (Show)

instance FromJSON Track where
  parseJSON (Object v) =
    Track <$>
    (v .: "name") <*>
    (v .: "startingPoint") <*>
    (v .: "pieces") <*>
    (v .: "lanes")

data RaceSession = RaceSession {
  laps :: Maybe Int,
  durationMs :: Maybe Int,
  maxLapTimeMs :: Maybe Int,
  quickRace :: Maybe Bool
} deriving (Show)

instance FromJSON RaceSession where
  parseJSON (Object v) =
    RaceSession <$>
    (v .:? "laps") <*>
    (v .:? "durationMs") <*>
    (v .:? "maxLapTimeMs") <*>
    (v .:? "quickRace")


data Race = Race {
  track :: Track,
  cars  :: [Car],
  raceSession :: RaceSession
} deriving (Show)

instance FromJSON Race where
  parseJSON (Object v) =
    Race <$>
    (v .: "track") <*>
    (v .: "cars") <*>
    (v .: "raceSession")

data GameInitData = GameInitData {
  race :: Race
} deriving (Show)

instance FromJSON GameInitData where
  parseJSON (Object v) =
    GameInitData <$>
    (v .: "race")



data CarLane = CarLane {
  startLaneIndex :: Int,
  endLaneIndex   :: Int
} deriving (Show)

instance FromJSON CarLane where
  parseJSON (Object v) =
    CarLane <$>
    (v .: "startLaneIndex") <*>
    (v .: "endLaneIndex")

data PiecePosition = PiecePosition {
  pieceIndex      :: Int,
  inPieceDistance :: Float,
  lane            :: CarLane,
  lap             :: Int
} deriving (Show)

instance FromJSON PiecePosition where
  parseJSON (Object v) =
    PiecePosition <$>
    (v .: "pieceIndex")      <*>
    (v .: "inPieceDistance") <*>
    (v .: "lane")            <*>
    (v .: "lap")

data CarPosition = CarPosition {
  carPosCarId   :: CarId,
  carPosAngle   :: Float,
  piecePosition :: PiecePosition
} deriving (Show)

instance FromJSON CarPosition where
  parseJSON (Object v) =
    CarPosition <$>
    (v .: "id") <*>
    (v .: "angle") <*>
    (v .: "piecePosition")

data GameResult = GameResult
                  deriving Show
instance FromJSON GameResult where
  parseJSON _ = return GameResult

data LapResult = LapResult
                 deriving Show
instance FromJSON LapResult where
  parseJSON _ = return LapResult

data DNFData = DNFData CarId Reason
               deriving Show
instance FromJSON DNFData where
  parseJSON (Object v) =
    DNFData <$>
    (v .: "car") <*>
    (v .: "reason")

data TurboData = TurboData {
    turboDurationMilliSeconds :: Float
  , turboDurationTicks :: Int
  , turboFactor :: Float
  } deriving (Show)

instance FromJSON TurboData where
  parseJSON (Object v) =
    TurboData <$>
    (v .: "turboDurationMilliseconds") <*>
    (v .: "turboDurationTicks") <*>
    (v .: "turboFactor")
  
