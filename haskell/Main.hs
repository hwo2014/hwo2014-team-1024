{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleInstances #-}

module Main where

import System.Environment (getArgs)
import System.Exit (exitFailure)

import Network(connectTo, PortID(..))
import System.IO(hPutStrLn, hGetLine, hSetBuffering, BufferMode(..), Handle)
import Data.List hiding (findIndex)
import Control.Monad
import Control.Applicative
import qualified Data.ByteString.Lazy.Char8 as L hiding (snoc)
import Data.Aeson(decode, FromJSON(..), fromJSON, parseJSON, eitherDecode, Value(..), (.:), Result(..))
import Data.Text (Text, pack, unpack)
import Control.Concurrent
import Control.Concurrent.MVar
import Control.Monad.Trans.State
import Control.Monad.Trans.Class
import Data.Vector as V (Vector, (!), length, singleton, update, findIndex, snoc)

import Message

type Velocity = Float
type Acceleration = Float
type AngularVel = Float
type DriftAngle = Float

connectToServer server port = connectTo server (PortNumber (fromIntegral (read port :: Integer)))

main = do
  args <- getArgs
  case args of
    [server, port, botname, botkey] -> run server port botname botkey
    _ -> do
      putStrLn "Usage: hwo2014bot <host> <port> <botname> <botkey>"
      exitFailure

run server port botname botkey = do
  h <- connectToServer server port
  hSetBuffering h LineBuffering
  hPutStrLn h $ botMessageJson $ Join (pack botname) (pack botkey)
  -- hPutStrLn h $ botMessageJson $ JoinTestRace (pack botname) (pack botkey) "germany" "1"
  -- hPutStrLn h $ botMessageJson $ JoinTestRace (pack botname) (pack botkey) "usa" "1"
  -- hPutStrLn h $ botMessageJson $ JoinTestRace (pack botname) (pack botkey) "france" "1"
  -- hPutStrLn h $ botMessageJson $ JoinTestRace (pack botname) (pack botkey) "elaeintarha" "1"
  -- hPutStrLn h $ botMessageJson $ JoinTestRace (pack botname) (pack botkey) "imola" "1"
  -- hPutStrLn h $ botMessageJson $ JoinTestRace (pack botname) (pack botkey) "england" "1"
  -- hPutStrLn h $ botMessageJson $ JoinTestRace (pack botname) (pack botkey) "suzuka" "1"
  servMVar <- newEmptyMVar
  botMVar <- newEmptyMVar
  stid <- forkIO $ serverHandler h servMVar
  btid <- forkIO $ botHandler h servMVar botMVar
  botExitMsg <- takeMVar botMVar
  print botExitMsg
  print "Stopping bot..."
  killThread stid
  killThread btid
  
serverHandler :: Handle -> MVar ServerMessage -> IO ()
serverHandler h servMVar = forever $ do
  msg <- hGetLine h
  case decode (L.pack msg) of
    Just json ->
      let decoded = fromJSON json >>= decodeServerMsg in
      case decoded of
        Success serverMsg -> putMVar servMVar serverMsg
        Error s -> fail $ "Error decoding message: " ++ s
    Nothing -> fail $ "Error parsing JSON: " ++ show msg

botHandler :: Handle -> MVar ServerMessage -> MVar Text -> IO ()
botHandler h servMVar botMVar = do
                                _ <- takeMVar servMVar
                                yourCar <- takeMVar servMVar
                                case yourCar of
                                  (YourCar carId) -> evalStateT runBotHandler (botStateInit {bsCarId = carId})
                                  _ -> sendMsg h Ping -- error "Expecting YourCar message"
                               
  where runBotHandler :: StateT BotState IO ()
        runBotHandler = do
          servMsg <- lift $ takeMVar servMVar
          case servMsg of
            CarPositions carPositions -> do
              bs <- get
              let carId = bsCarId bs
              case find (\(CarPosition carId' _ _) -> carId' == carId) carPositions of
                   Just cps -> do
                       -- lift $ maybe (return ()) (\pp -> print $ (pieceLength ((pieces track') ! (pieceIndex pp)) $ cor ((pieces track') ! (pieceIndex pp))  , pieceIndex pp)) prevPP   
                       modify (\(BotState {}) -> fst newbs)
                       lift $ print $ (show . pieceIndex . piecePosition $ cps)
                                      ++ ", " ++ (show . bsAng . bsPhy . fst $ newbs)
                                      ++ ", " ++ (show . bsVel . bsPhy . fst $ newbs)
                                      -- ++ ", " ++ (show . snd $ newbs)
                       sendThrt . snd $ newbs
                       where newbs = gameLogic bs cps
                             sendThrt t = lift . sendMsg h $ Throttle t
                   Nothing -> lift $ sendMsg h Ping --error "Car gone missing" 
            Unknown msgType -> do
                               lift $ putStrLn $ "Unknown message: " ++ show msgType
                               lift $ sendMsg h Ping
            DNF _ -> lift $ sendMsg h Ping -- lift $ putMVar botMVar "Disqualified"
            TournamentEnd -> lift $ putMVar botMVar "Tournament completed!"
            GameInit gameInitData -> gameInit gameInitData
            unk -> do
                   lift $ putStrLn $ "fail at " ++ show unk
                   lift $ sendMsg h Ping
          runBotHandler

gameInit :: GameInitData -> StateT BotState IO ()
gameInit gameInitData = do
  lift $ print $ "Received GameInit"
  lift $ print $ gameInitData
  lift $ print "Starting game ... "
  modify (\bs@(BotState{bsCarId = carId''}) -> 
             case find (\(Car carId' _) -> carId' == carId'') (cars . race $ gameInitData) of
               Just (Car _ cDim) -> bs {
                      bsCarId = carId''
                    , bsTrack = (track . race $ gameInitData)
                    , bsCarDim = cDim
                   }
               _ -> error "Expected one matching car with specified Id")

gameLogic :: BotState -> CarPosition -> (BotState, Float)
gameLogic oldbs cps = (newBs, newThrt)
  where newPhy = newBotPhy oldbs cps
        drftCur = bsSign newPhy
        velCur = case bsVel newPhy of
                   v | v < 6.5 -> GRN
                     | (v >= 6.5) && (v < 8.5) -> ORNG
                     | otherwise -> RED
        curPiece' = curPiece (bsTrack oldbs) cps
        nextPiece = nextNthPiece (bsTrack oldbs) cps 1
        nPlus2 = nextNthPiece (bsTrack oldbs) cps 2
        isSharpBend r = r <= 60.0
        verySlow = (bsVel newPhy) < 5.0
        newThrt = case (curPiece', nextPiece, nPlus2) of
                      (Straight {}, Straight {}, Bend {radius=r}) -> 
                                if isSharpBend r -- sharp bend ahead strict check on vel
                                  then case verySlow of
                                        True -> 1.0
                                        False -> 0.7
                                  else case verySlow of
                                        True -> 1.0
                                        False -> 0.95
                      (Straight {}, Bend {radius=r},_) ->  -- bend ahead check on vel
                                if isSharpBend r
                                  then case (verySlow,drftCur) of
                                           (True,GRN) -> 0.5
                                           _ -> 0.0000000000001
                                  else case (verySlow,drftCur) of -- normal bend
                                           (True,GRN) -> 0.65
                                           _ -> 0.00000000001
                      (Bend {radius=r1}, Bend {radius=r2},_) -> -- meandering check vel and drift
                                if (isSharpBend r1) || (isSharpBend r2)
                                  then case (verySlow,velCur,drftCur) of
                                           (True,GRN,GRN) -> 0.65
                                           _ -> 0.00000000000001
                                  else case (verySlow,velCur,drftCur) of -- normal bend
                                           (True,GRN,GRN) -> 1.0
                                           (_,GRN,GRN) -> 0.98
                                           _ -> 0.0000000001
                      (Bend {radius=r}, Straight {},_) -> 
                                if isSharpBend r
                                  then case (verySlow,velCur,drftCur) of
                                           (True,GRN,GRN) -> 0.65
                                           _ -> 0.0000000000001
                                  else case (verySlow,velCur,drftCur) of
                                           (True,GRN,GRN) -> 1.0
                                           (_,_,GRN) -> 0.9
                                           _ -> 0.000000001
                      (Straight {}, Straight {},_) -> 1.0
        newBs = oldbs {
            bsLastPiecePos = Just $ piecePosition cps
          , bsPhy = newPhy
          }



newBotPhy :: BotState -> CarPosition -> BotPhy
newBotPhy bs cps = 
  (bsPhy bs) {
             bsSign = driftSignal (carPosAngle cps)
           , bsAng = (carPosAngle cps) 
           , bsAngVel = case curPiece' of
                           Straight {} -> (bsAngVel . bsPhy $ bs)
                           Bend {pieceAngle=pA} ->  angVel pA
           , bsVel = velocity' 
           , bsAccl = acceleration'
           }
  where curPiece' = curPiece (bsTrack bs) cps
        nextPiece = nextNthPiece (bsTrack bs) cps 1
        velocity' = velocity cps bs
        acceleration' = velocity' - (bsVel . bsPhy $ bs)
        angVel pA = sin pA * velocity' / (radius curPiece' + cor curPiece') 
        corDist = distanceFromCenter ((lanes . bsTrack $ bs) ! (startLaneIndex . lane . piecePosition $ cps))
        cor pp = fromIntegral $ case pp of
          Straight {} -> 0
          Bend {pieceAngle=pA} | pA >= 0  -> corDist * (-1)
                               | otherwise -> corDist
        driftSignal carang = 
              case ptive carang of
                  ang | ang < 20.0 -> GRN
                      | (ang >= 20.0) && (ang < 25.0) -> ORNG
                      | otherwise -> RED
        ptive n = if n < 0 
                    then n * (-1)
                    else n

-- percChangePhy :: BotPhy -> BotPhy -> BotPhy
-- percChangePhy oldPhy newPhy = newPhy {
--                                   bsAng = pCh (bsAng oldPhy) (bsAng newPhy)
--                                 , bsVel = pCh (bsVel oldPhy) (bsVel newPhy)
--                                 , bsAngVel = pCh (bsAngVel oldPhy) (bsAngVel newPhy)
--                                 , bsAccl = pCh (bsAccl oldPhy) (bsAccl newPhy)
--                                 }
--     where pCh oldN newN = if oldN == 0.0
--                             then ((newN - 0.00001) / 0.00001) * 100
--                             else ((newN - oldN) / oldN) * 100

curPiece :: Track -> CarPosition -> Piece
curPiece track cps = (pieces track) ! (pieceIndex . piecePosition $ cps)

nextNthPiece, prevNthPiece :: Track -> CarPosition -> Int -> Piece
nextNthPiece track cps n = (pieces track) ! ((n + (pieceIndex $ piecePosition cps)) `mod` vecLen)
  where vecLen = V.length $ pieces track
prevNthPiece track cps n = (pieces track) ! ((n - (pieceIndex $ piecePosition cps)) `mod` vecLen)
  where vecLen = V.length $ pieces track

velocity :: CarPosition -> BotState -> Float
velocity cps bs =
  let track' = bsTrack bs
      prevPP = bsLastPiecePos bs 
      corDist = distanceFromCenter ((lanes track') ! (startLaneIndex . lane . piecePosition $ cps))
      cor pp = fromIntegral $ case pp of
          Straight {} -> 0
          Bend {pieceAngle=pA} | pA >= 0  -> corDist * (-1)
                               | otherwise -> corDist
  in
    case prevPP of
        Just pp -> case ((inPieceDistance pp) > (inPieceDistance $ piecePosition cps)) of
                      True -> let prevPieceLen = (pieceLength ((pieces track') ! (pieceIndex pp))) $ cor ((pieces track') ! (pieceIndex pp))
                              in (prevPieceLen - (inPieceDistance pp)) + (inPieceDistance $ piecePosition cps)
                      False -> (inPieceDistance $piecePosition cps) - (inPieceDistance $ pp)
        Nothing -> inPieceDistance $ piecePosition $ cps
  
sendMsg :: Handle -> BotMessage -> IO ()
sendMsg h msg = hPutStrLn h $ botMessageJson msg

decodeServerMsg :: (Text, Value) -> Result ServerMessage
decodeServerMsg (msgType, msgData)
  | msgType == "join" = return Joined
  | msgType == "gameInit" = GameInit <$> (fromJSON msgData)
  | msgType == "carPositions" = CarPositions <$> (fromJSON msgData)
  | msgType == "yourCar" = YourCar <$> (fromJSON msgData)
  | msgType == "gameStart" = return GameStart
  | msgType == "gameEnd" = GameEnd <$> (fromJSON msgData)
  | msgType == "tournamentEnd" = return TournamentEnd
  | msgType == "crash" = Crash <$> (fromJSON msgData)
  | msgType == "spawn" = Spawn <$> (fromJSON msgData)
  | msgType == "lapFinished" = LapFinished <$> (fromJSON msgData)
  | msgType == "dnf" = DNF <$> (fromJSON msgData)
  | msgType == "finish" = Finish <$> (fromJSON msgData)
  | msgType == "turboAvailable" = Turbo <$> (fromJSON msgData)                        
  | otherwise = return $ Unknown msgType

instance FromJSON a => FromJSON (Text, a) where
  parseJSON (Object v) = do
    msgType <- v .: "msgType"
    msgData <- v .: "data"
    return (msgType, msgData)
  parseJSON x          = fail $ "Not an JSON object: " ++ (show x)


botMessageJson :: BotMessage -> String
botMessageJson (Join n k) = "{\"msgType\":\"join\",\"data\":{\"name\":\"" ++ (unpack n) ++ "\",\"key\":\"" ++ (unpack k) ++ "\"}}"
botMessageJson (JoinTestRace n k t cn) ="{\"msgType\": \"joinRace\", \"data\": {\"botId\": {\"name\": \"" ++ (unpack n) ++ "\",\"key\": \"" ++ (unpack k) ++ "\"},\"trackName\":\"" ++ (unpack t) ++"\",\"carCount\": "++ (unpack cn) ++ "}}"
botMessageJson (Throttle t) = "{\"msgType\":\"throttle\",\"data\":" ++ (show t) ++ "}"
botMessageJson Ping = "{\"msgType\":\"ping\",\"data\":{}}"
botMessageJson (SwitchingLane SwitchLeft) = "{\"msgType\":\"switchLane\",\"data\":\"Left\"}"
botMessageJson (SwitchingLane SwitchRight) = "{\"msgType\":\"switchLane\",\"data\":\"Right\"}"
data BotPhy = BotPhy {
                    bsSign :: Signal
                  , bsAng :: DriftAngle
                  , bsAngVel :: AngularVel
                  , bsVel :: Velocity
                  , bsAccl :: Acceleration
    } deriving Show

data BotState = BotState { 
                    bsCarId :: CarId
                  , bsTrack :: Track
                  , bsCarDim :: Dimension
                  , bsLastPiecePos :: Maybe PiecePosition
                  , bsHue :: Bool  
                  , bsPhy :: BotPhy
                  -- , bsEvenLapPhy :: Vector (Int,BotPhy) --pieceid,phy
                  -- , bsOddLapPhy :: Vector (Int,BotPhy)
                  } deriving Show
data Signal = RED | ORNG | GRN deriving Show

botPhy0 :: BotPhy
botPhy0 = BotPhy GRN 0.0 0.0 0.0 0.0

botStateInit :: BotState
botStateInit = BotState {
     bsCarId = error "Bot State not initialize properly"
   , bsTrack = error "Bot State not initialize properly"
   , bsCarDim = error "Bot State not initialize properly"
   , bsLastPiecePos = Nothing
   , bsHue = False                   
   , bsPhy = botPhy0
   -- , bsEvenLapPhy = empty
   -- , bsOddLapPhy = empty
   }
